# Arduino Wi-Fi Weather station #

Arduino-based device to measure temperature and humidity using **Adafruit HUZZAH ESP8266** with **DHT-22** as a sensor

### Setup ###

+ **Items needed**
    * [Adafruit HUZZAH ESP8266](https://www.adafruit.com/product/2471)
    * [DHT22](https://www.adafruit.com/product/385) temperature and humidity sensor ([DHT11](https://www.adafruit.com/product/386) should work as well)
    * Breadbord, wires
    * 10 kOhm resistor

+ **Circuit setup**
    * Check the [Fritzing](http://fritzing.org/home/) diagrams in the ".../schema" folder

+ **Configuration**
    + [Arduino IDE](https://www.arduino.cc/en/main/software)

+ **Deployment instructions**
    * Following the schematics, connect DHT22 to ESP8266 using a 10 kOhm pull-up resistor

    ![schema_1.jpg](https://bitbucket.org/repo/BgkpjKy/images/1460677336-schema_1.jpg)
    ![schema_2.jpg](https://bitbucket.org/repo/BgkpjKy/images/3254519721-schema_2.jpg)   

    * Connect ESP8266 to PC via USB
    * Open Arduino IDE and install all the necessary **libraries**
        * ESP8266WebServer
        * ESP8266WiFi
        * ESP8266WiFiMesh
        * DHT sensor library

        ![tutorial2.png](https://bitbucket.org/repo/BgkpjKy/images/742845456-tutorial2.png)
        ![tutorial3.png](https://bitbucket.org/repo/BgkpjKy/images/1605175385-tutorial3.png)

    * Open the project file in Arduino IDE
    * Upload the web server files using "**Tools -> ESP8266 Sketch Data Upload**"

    ![tutorial1.png](https://bitbucket.org/repo/BgkpjKy/images/525165762-tutorial1.png)

    * Compile the project and upload it to your ESP8266 device
    * Find a new WiFi hot-spot called "**ESP8266-**" (name and part of its MAC-address) and connect to it with the same password you used in the code (default: **sparkfun**)

    ![wifi-pwd.PNG](https://bitbucket.org/repo/BgkpjKy/images/4185209848-wifi-pwd.PNG)

    ![wifi_guide.jpg](https://bitbucket.org/repo/BgkpjKy/images/2546159950-wifi_guide.jpg)

    * Open a browser and go to **[http://192.168.4.1](http://192.168.4.1)** (default IP)

    ![result.png](https://bitbucket.org/repo/BgkpjKy/images/2101174825-result.png)

### Information Sources ###
* Adafruit HUZZAH ESP8266 Datasheets
    + https://learn.adafruit.com/adafruit-huzzah-esp8266-breakout/downloads
* DHT22 Datasheet
    + https://cdn-shop.adafruit.com/datasheets/DHT22.pdf
* ESP8266 Web Server with Arduino IDE
    + http://randomnerdtutorials.com/esp8266-web-server-with-arduino-ide/

### Built With ###
* Arduino IDE

### Frontend part uses ###
* [jquery](https://jquery.com/)
* [highcharts](https://www.highcharts.com/)

### License ###
This project is licensed under the MIT License