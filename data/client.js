var chart; // global

function requestData() {
    $.ajax({
        url: './json',
        success: function(point) {
			var date = new Date();
			var time = date.getTime();
			
            var series = chart.series[0],
                shift = series.data.length > 20; // shift if the series is 
                                                 // longer than 20
			var pointObj = JSON.parse(point);
			console.log(pointObj[0])
												 
            // add the point
            chart.series[0].addPoint([time, pointObj[0]/100], true, shift);
			chart.series[1].addPoint([time, pointObj[1]/100], true, shift);
            
            // call it again after one second
            setTimeout(requestData, 1000);    
        },
        cache: false
    });
}


Highcharts.setOptions({
   global: {
       useUTC: false
   }
});

chart = new Highcharts.chart('container', {
        chart: {
			events: {
                load: requestData
            }
        },
		rangeSelector: {
			buttons: [{
				count: 1,
				type: 'minute',
				text: '1M'
			}, {
				count: 5,
				type: 'minute',
				text: '5M'
			}, {
				type: 'all',
				text: 'All'
			}],
			inputEnabled: false,
			selected: 0
		},
		exporting: {
			enabled: false
		},
        title: {
            text: 'ESP8266 Temperature'
        },
        subtitle: {
            text: 'Source: DHT22'
        },
        xAxis: {
            type: 'datetime',
			tickPixelInterval: 150,
			maxZoom: 20 * 1000
        },
        yAxis: {
            title: {
                text: 'Value'
            }
		},
		plotOptions: {
			line: {
				dataLabels: {
					enabled: true
				},
				enableMouseTracking: false
			}
		},
		tooltip: {
			shared: true
		},
        series: [{
            name: 'Temperature',
            data: [],
			tooltip: {
				valueSuffix: '°C'
			},
			dataLabels: {
				formatter: function () {
						return this.y + '°C';
				}
			}
        },
		{
            name: 'Humidity',
            data: [],
			tooltip: {
				valueSuffix: '%'
			},
			dataLabels: {
				formatter: function () {
						return this.y + '%';
				}
			}
        }]
});
